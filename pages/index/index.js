//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    dataNum: { friendsWord:3,classMateLike:135,friendDown:13},
    wordMsg: [{
      name:"会吃鱼的猫",
      word:"好久不见啊聪哥最近还好吗？你现在在哪里上班啊。有空去你那边玩下。",
      date:"2017-06-15"
    }, {
      name: "放风筝的人",
      word: "聪哥最近还好吗？有空出来吃烤鱼啊，我请客。",
      date: "2017-06-17"
      }, {
        name: "流浪的孩子",
        word: "好久不见冒泡了，最近都干嘛去了啊？你是不是很久都不上QQ了？",
        date: "2017-06-29"
      }]
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  friendsDownFn: function () {
    var friendsLike = this.data.dataNum.friendDown;
    var num = this.data.dataNum;
    num.friendDown++;
    console.log(num.friendDown);
    this.setData({
      dataNum: num
    });
    wx.setStorage({
      key: "dataNum",
      data: JSON.stringify(num)
    });
  },
  classMate:function(){
    var friendsLike = this.data.dataNum.classMateLike;
    var num = this.data.dataNum;
    num.classMateLike++;
    console.log(num.classMateLike);
    this.setData({
      dataNum: num
    });
    wx.setStorage({
      key: "dataNum",
      data: JSON.stringify(num)
    });
  },
  onLoad: function () {
    var that = this;   
    wx.getStorage({
      key: 'wordMsg',
      success: function (res) {
        var num = that.data.dataNum;
        var array = JSON.parse(res.data);
        var concatArray = array.concat(that.data.wordMsg);
        num.friendsWord = concatArray.length;
        that.setData({
          dataNum: num,
          wordMsg: concatArray
        });

      }
    });


    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      wx.getStorage({
        key: 'dataNum',
        success: function (res) {
         
        }
      });
    })
  }
})
