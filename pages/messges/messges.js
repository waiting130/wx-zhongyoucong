// pages/messges/messges.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wordMsg:{name:"", word:"", date:""},
    formvalue:{name:"",word:""}
  },
  getName:function(e){
    var wordMsg = this.data.wordMsg;
    wordMsg.name = e.detail.value;
    this.setData({
      wordMsg: wordMsg
    });
  },
  getWord: function (e) {
    var wordMsg = this.data.wordMsg;
    wordMsg.word = e.detail.value;
    this.setData({
      wordMsg: wordMsg
    });
  },
  submit:function(){
    var that = this;
    //日起格式化函数
    function getNowFormatDate() {
      var date = new Date();
      var seperator1 = "-";
      var seperator2 = ":";
      var month = date.getMonth() + 1;
      var strDate = date.getDate();
      if (month >= 1 && month <= 9) {
        month = "0" + month;
      }
      if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
      }
      var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds();
      return currentdate;
    }
    if (this.data.wordMsg.name == "" || this.data.wordMsg.word == ""){
      wx.showToast({
        title: '请填写姓名和留言',
        icon: 'success',
        duration: 2000
      });
    }else{
      var wordMsg = this.data.wordMsg;
      var date = getNowFormatDate().split(" ")[0];//获取日期
      wordMsg.date = date;
      var finalData;
      wx.getStorage({
        key: 'wordMsg',
        success: function (res) {
          finalData = JSON.parse(res.data);
          finalData.unshift(wordMsg);
          wx.setStorage({
            key: "wordMsg",
            data: JSON.stringify(finalData)
          });
          wx.showToast({
            title: '留言成功',
            icon: 'success',
            duration: 3000,
            success: function () {
              wx.switchTab({
                url: '../index/index',
                success: function (e) {           
                  var page = getCurrentPages().pop();
                  if (page == undefined || page == null) return;
                  page.onLoad();

                } 
              });
            }
          });
        }
      }); 
    }
    
    
    

    console.log(getNowFormatDate());
    /**/
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorage({
      key: "wordMsg",
      data: "[]"
    });
  },

 

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})